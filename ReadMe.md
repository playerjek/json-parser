This app is using for parsing invalid json (unexpected double quotes present). <br>
To run app use java -jar command with 1 arg - its path to file with input json. <br>
Check test package to see examples of parsing. <br>
Another way to do this task replace double quotes with single and then org.json can parse input structure without any manipulation,<br>
so 1 line of code: <code>new JSONObject(json.replaceAll("\"", "'"))</code><br>