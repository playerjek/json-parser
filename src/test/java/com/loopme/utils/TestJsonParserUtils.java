package com.loopme.utils;

import com.loopme.utils.JsonParserUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

/**
 * @author IAsmolov on 10.07.17.
 */
@RunWith(Parameterized.class)
public class TestJsonParserUtils {

    private String json;

    public TestJsonParserUtils(String json) {
        this.json = json;
    }

    @Test
    public void testParseJson(){
        System.out.println(json);
        Assert.assertNotNull(JsonParserUtils.parseJsonWithEscapingQuotes(json));
    }

    @Parameterized.Parameters
    public static Collection<Object[]> goodCases() {
        return Arrays.asList(new Object[][]{
                {"{\"id\": \"123\",\n  \"ua\": \"Mozilla/firefox, lg4.5\" 1.2123\"}"},
                {"{\"id\": \"123\",\n  \"ua\": \"Mozilla/firefox,\"\" lg4.5\" 1.2123\"}"},
                {"{\"id\": \"123\",\n  \"ua\": \"Mozilla/firefox, lg4.5\"\" 1.2123\"}"},
                {"{\"id\": \"1\"23\",\n  \"ua\": \"Mozilla/firefox, lg4.5\" 1.2123\"}"},
                {"{\"id\": \"123\",\n  \"ua\": \"Mozilla/firefox, lg4.5\" 1.2123\"\"}"},
                {"{\"id\": \"123\",\n  \"ua\": \"Mozilla/firefox, lg4.5\" \"\"1.2123\"}"},
        });
    }
}
