package com;

import com.loopme.utils.FileReaderUtils;
import com.loopme.utils.JsonParserUtils;
import org.json.JSONObject;

public class JsonParser {

	public static void main(String[] args) {
		String jsonContent = FileReaderUtils.readAndValidateInputFile(args);
		JSONObject parsedJson = JsonParserUtils.parseJsonViaOrgJson(jsonContent);

		if (parsedJson == null) {
			parsedJson = JsonParserUtils.parseJsonWithEscapingQuotes(jsonContent);
			if (parsedJson == null) {
				System.err.println("Json has wrong format, unable to parse :(");
				System.exit(4);
			}
		}
		System.out.println("Json parsed successfully :) : " + parsedJson.toString());
		System.exit(0);
	}
}
