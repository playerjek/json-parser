package com.loopme.utils;

import org.apache.commons.lang.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by playe_000 on 10.07.2017.
 */
public class FileReaderUtils {

    public static String readAndValidateInputFile(String[] args) {
        if (args == null || StringUtils.isEmpty(args[0])) {
            System.err.println("You should specify path to file with json [*.json]");
            System.exit(1);
        }
        byte[] jsonFile = new byte[0];
        try (FileInputStream fis = new FileInputStream(args[0])) {
            jsonFile = new byte[fis.available()];
            int readed = fis.read(jsonFile);
            if (readed <= 0) {
                System.err.println("File " + args[0] + " is empty");
                System.exit(2);
            }
        } catch (IOException e) {
            System.err.println("File " + args[0] + " is broken or not exist");
            System.exit(3);
        }

        return new String(jsonFile);
    }
}
