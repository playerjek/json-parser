package com.loopme.utils;

import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;

public class JsonParserUtils {

    private static final String KEY_REGEXP = "\"[a-z]+\"";

    /**
     * Parse json with org.json
     * @param json
     * @return parsed JSONObject or null
     */
    public static JSONObject parseJsonViaOrgJson(String json) {
        try {
            JSONObject jsonObject = new JSONObject(json);
            return jsonObject;
        } catch (JSONException e) {
            System.err.println("Parse with org.json was failed - json has wrong format");
        }
        return null;
    }

    /**
     * Parse json and escape illegal quotes
     * @param jsonContent
     * @return parsed JSONObject or null
     */
    public static JSONObject parseJsonWithEscapingQuotes(String jsonContent) {
        String[] nodes = jsonContent.split(KEY_REGEXP);
        for (String node: nodes) {
            int quotes = StringUtils.countMatches(node, "\"");
            String fixedNote = node;
            if (quotes > 2) {
                for (int i=2; i <= quotes - 1; i++) {
                    int qouteIndex = StringUtils.ordinalIndexOf(fixedNote, "\"", i);
                    fixedNote = fixedNote.substring(0, qouteIndex) + "\\\"" + fixedNote.substring(qouteIndex+1, fixedNote.length());
                }
                jsonContent = jsonContent.replace(node, fixedNote);
            }
        }
        return parseJsonViaOrgJson(jsonContent);
    }
}
